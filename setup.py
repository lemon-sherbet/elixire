# elixire: Image Host software
# Copyright 2018, elixi.re Team and the elixire contributors
# SPDX-License-Identifier: AGPL-3.0-only

from setuptools import setup

setup(
    name='elixire',
    version='2018.2.0.0',
    description='Image host',
    url='https://elixi.re',
    author='Ave Ozkal, Luna Mendes, Mary Strodl, slice',
    python_requires='>=3.6'
)
